<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Stringable;

/**
 * IntegerCapacityInterface interface file.
 * 
 * An integer capacity represents an range of integers that can be represented
 * numerically, either using integers or multiple integers, or strings.
 * 
 * @author Anastaszor
 */
interface IntegerCapacityInterface extends Stringable
{
	
	/**
	 * Gets the number of bits (rounded up) this capacity needs for all values
	 * to be correctly represented.
	 * 
	 * @return integer
	 */
	public function getNumberOfBits() : int;
	
	/**
	 * Gets the number of bytes (rounded up) this capacity needs for all values
	 * to be correctly represented.
	 * 
	 * @return integer
	 */
	public function getNumberOfBytes() : int;
	
	/**
	 * Gets the number of digits (rounded up) this capacity needs for all
	 * values to be correctly represented.
	 * 
	 * @return integer
	 */
	public function getNumberOfDigitsBase10() : int;
	
	/**
	 * Gets the number of hexadecimal digits (rounded up) this capacity needs
	 * for all values to be correctly represented.
	 * 
	 * @return integer
	 */
	public function getNumberOfDigitsBase16() : int;
	
	/**
	 * Gets a string that represents the minimum value when this capacity is
	 * used as signed storage.
	 * 
	 * @return string
	 */
	public function getSignedMinimumValue() : string;
	
	/**
	 * Gets a string that represents the maximum value when this capacity is
	 * used as signed storage.
	 * 
	 * @return string
	 */
	public function getSignedMaximumValue() : string;
	
	/**
	 * Gets a string that represents the maximum value when this capacity is
	 * used as unsigned storage.
	 * 
	 * @return string
	 */
	public function getUnsignedMaximumValue() : string;
	
	/**
	 * Merges this integer capacity with the other integer capacity into a new
	 * integer capacity object. The new capacity will represent the minimal 
	 * capacity that is able to contain both capacities.
	 * 
	 * @param IntegerCapacityInterface $capacity
	 * @return IntegerCapacityInterface
	 */
	public function mergeWith(IntegerCapacityInterface $capacity) : IntegerCapacityInterface;
	
}
